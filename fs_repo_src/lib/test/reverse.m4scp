m4include(m4define([m4currentFile],builtin(translit,__file__,\,/))builtin(regexp,m4currentFile,.*[/],\&)_config.m4)
M4SCS(
#include "../../include/scp_keynodes.scsy"
// Location: /lib/test/reverse
)

procedure(test_reverse,
M4SCS(	[[
			chain_push_singlet = "/lib/chain/push_singlet/chain_push_singlet",
			chain_push_first_singlet = "/lib/chain/push_first_singlet/chain_push_first_singlet",
            chain_get_next_singlet = "/lib/chain/get_next_singlet/chain_get_next_singlet",
			chain_pop_destroy_singlet = "/lib/chain/pop_destroy_singlet/chain_pop_destroy_singlet",
            enter_message = /"\nchain_reverse: entering\n"/,
            leave_message = /"\nchain_reverse: leaving\n"/,
			prm0, prm1, prm2, prm3, prm4, prm5,
			prmp0, prmp1, prmp2, prmp3, prmp4, prmp5, prmp6, prmp7, prmp8
		]]),
		[{  
			logger, descr, chain, first_value, value, empty,
            reversed_chain, state, true, stack, then_routine,
            then_parameters, else_routine, else_parameters, 
            parameters, modified_parameters,
            then_callback_chain, else_callback_chain,
            modified_then_callback_chain, modified_else_callback_chain
		}],
		{[
            1_: in_: out_: state,
            2_: in_: out_: true,
            3_: in_: out_: stack,
            4_: in_: parameters, 
            5_: in_: logger
		]})

DBG(printNl([1_: enter_message]))

genEl([1_: assign_: const_: node_: logger])

genEl([1_: assign_: node_: const_: empty])

call([
    1_: fixed_: chain_get_next_singlet, 
    2_: fixed_: prm0 = [{
        1_: parameters,
        2_: modified_parameters,
        3_: else_callback_chain
    }], 
    3_: assign_: descr
])

waitReturn([1_: fixed_: descr])

call([
    1_: fixed_: chain_get_next_singlet, 
    2_: fixed_: prm1 = [{
        1_: modified_parameters,
        2_: modified_parameters,
        3_: then_callback_chain
    }], 
    3_: assign_: descr
])

waitReturn([1_: fixed_: descr])

call([
    1_: fixed_: chain_get_next_singlet, 
    2_: fixed_: prm2 = [{
        1_: else_callback_chain,
        2_: modified_else_callback_chain,
        3_: else_parameters
    }], 
    3_: assign_: descr
])

waitReturn([1_: fixed_: descr])

call([
    1_: fixed_: chain_get_next_singlet, 
    2_: fixed_: prm3 = [{
        1_: modified_else_callback_chain,
        2_: modified_else_callback_chain,
        3_: else_routine
    }], 
    3_: assign_: descr
])

waitReturn([1_: fixed_: descr])


call([
    1_: fixed_: chain_get_next_singlet, 
    2_: fixed_: prm4 = [{
        1_: then_callback_chain,
        2_: modified_then_callback_chain,
        3_: then_parameters
    }], 
    3_: assign_: descr
])

waitReturn([1_: fixed_: descr])

call([
    1_: fixed_: chain_get_next_singlet, 
    2_: fixed_: prm5 = [{
        1_: modified_then_callback_chain,
        2_: modified_then_callback_chain,
        3_: then_routine
    }], 
    3_: assign_: descr
])

waitReturn([1_: fixed_: descr])

call([
    1_: fixed_: chain_get_next_singlet, 
    2_: fixed_: prmp0 = [{1_: stack, 2_: stack, 3_: chain}], 
    3_: assign_: descr
])

waitReturn([1_: fixed_: descr])

ifVarAssign([1_: chain],, cs_erase_empty)

//==================REVERSE CHAIN========================

call([
    1_: fixed_: chain_get_next_singlet, 
    2_: fixed_: prmp1 = [{1_: chain, 2_: chain, 3_: first_value}],
    3_: assign_: descr
])

waitReturn([1_: fixed_: descr])

ifVarAssign([1_: first_value],, cs_chain_failed)

call([
    1_: fixed_: chain_push_first_singlet, 
    2_: fixed_: prmp2 = [{
        1_: first_value, 
        2_: reversed_chain, 
        3_: logger
    }],
    3_: assign_: descr
])

waitReturn([1_: fixed_: descr])

label(cs_reverse_element)
call([
    1_: fixed_: chain_get_next_singlet, 
    2_: fixed_: prmp3 = [{1_: chain, 2_: chain, 3_: value}],
    3_: assign_: descr
])

waitReturn([1_: fixed_: descr])

ifVarAssign([1_: value],, cs_end_chain_loop)

call([
    1_: fixed_: chain_push_singlet, 
    2_: fixed_: prmp4 = [{
        1_: reversed_chain, 
        2_: value, 
        3_: reversed_chain, 
        4_: logger
    }],
    3_: assign_: descr
])

waitReturn(
    [1_: fixed_: descr], 
    cs_reverse_element, 
    cs_reverse_element
)

label(cs_end_chain_loop)
ifVarAssign(
    [1_: chain],
    cs_chain_failed
)
//==================CHAIN CREATED=============================
call([
    1_: fixed_: chain_push_singlet, 
    2_: fixed_: prmp5 = [{
        1_: stack, 
        2_: reversed_chain, 
        3_: stack, 
        4_: logger
    }],
    3_: assign_: descr
])

waitReturn([1_: fixed_: descr])

ifVarAssign(
    [1_: then_parameters], 
    cs_then_parameters_defined
)

varAssign([1_: assign_: then_parameters, 2_: fixed_: empty])

label(cs_then_parameters_defined)
ifVarAssign(
    [1_: then_routine],,
    cs_then_routine_non_executable
)

ifCoin([
        1_: fixed_: then_routine,
        2_: fixed_: then_parameters
    ],
    cs_then_routine_non_executable
) 

call([
    1_: fixed_: then_routine, 
    2_: fixed_: prmp6 = [{
        1_: state, 
        2_: true, 
        3_: stack, 
        4_: then_parameters,
        5_: logger
    }],
    3_: assign_: descr
])

waitReturn([1_: fixed_: descr], cs_pop_chain, cs_pop_chain)

label(cs_then_routine_non_executable)
varAssign([1_: assign_: state, 2_: fixed_: true])

label(cs_pop_chain)
call([
    1_: fixed_: chain_pop_destroy_singlet, 
    2_: fixed_: prmp7 = [{
        1_: stack, 
        3_: stack, 
        4_: logger
    }],
    3_: assign_: descr
])

waitReturn([1_: fixed_: descr], cs_erase_empty, cs_erase_empty)

//==================CHAIN FAILED TO CREATE===================
label(cs_chain_failed)
ifVarAssign(
    [1_: else_parameters], 
    cs_else_parameters_defined
)
varAssign([1_: assign_: else_parameters, 2_: fixed_: empty])

label(cs_else_parameters_defined)
ifVarAssign(
    [1_: else_routine],,
    cs_erase_empty
)

ifCoin([
        1_: fixed_: else_routine,
        2_: fixed_: else_parameters
    ],
    cs_erase_empty
) 

call([
    1_: fixed_: else_routine, 
    2_: fixed_: prmp8 = [{
        1_: state, 
        2_: true, 
        3_: stack, 
        4_: else_parameters,
        5_: logger
    }],
    3_: assign_: descr
])

waitReturn([1_: fixed_: descr])
//=========================================================

label(cs_erase_empty)
eraseEl([1_: f_: fixed_: empty]) 

printEl([1_: fixed_: logger])

eraseEl([1_: f_: fixed_: logger])

DBG(printNl([1_: leave_message]))

return([])
end()